﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fractal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            init();
            destroy();
            start();
            stop();
        }
        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static Boolean action, rectangle, finished;
        private static float xy;
        //djm original Image picture; ---now replaced with PictureBox picture
        private PictureBox picture;
        private Graphics g1;
        private Cursor c1, c2;
        private Bitmap pictureBitmap;


        public void init()
        {
            this.Height = 400;
            this.Width = 640;
            finished = false;
            //addMouseListener(this);
            //addMouseMotionListener(this);
            //c1 = new Cursor(Cursor.WAIT_CURSOR);
            c1 = Cursors.WaitCursor;
            //c2 = new Cursor(Cursor.CROSSHAIR_CURSOR);
            c2 = Cursors.Cross;
            x1 = this.Width;
            y1 = this.Height;
            xy = (float)x1 / (float)y1;
            //picture = createImage(x1, y1);
            pictureBitmap = new Bitmap(x1, y1);
            //g1 = picture.getGraphics();
            g1 = Graphics.FromImage(pictureBitmap);
            finished = true;
        }

        public void destroy() // delete all instances 
        {
            if (finished)
            {
                // removeMouseListener(this);
                // removeMouseMotionListener(this);
                picture = null;
                g1 = null;
                c1 = null;
                c2 = null;
                System.GC.Collect(); // garbage collection
            }
        }

        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }

        public void stop()
        {
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            g1 = e.Graphics;
            g1.DrawImageUnscaled(pictureBitmap, 0, 0);
            if (rectangle)
            {
                //g.setColor(Color.White);
                Pen myPen = new Pen(Color.Red);
                if (xs < xe)
                {
                    if (ys < ye) g1.DrawRectangle(myPen, xs, ys, (xe - xs), (ye - ys));
                    else g1.DrawRectangle(myPen, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g1.DrawRectangle(myPen, xe, ys, (xs - xe), (ye - ys));
                    else g1.DrawRectangle(myPen, xe, ye, (xs - xe), (ys - ye));
                }
            }
        }

        public void paint(Graphics g)
        {
           update(g);
        }
        public void update(Graphics g)
        {
            g.DrawImageUnscaled(pictureBitmap, 0, 0);
            if (rectangle)
            {
                //g.setColor(Color.White);
                Pen myPen = new Pen(Color.White);
                if (xs < xe)
                {
                    if (ys < ye) g.DrawRectangle(myPen, xs, ys, (xe - xs), (ye - ys));
                    else g.DrawRectangle(myPen, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g.DrawRectangle(myPen, xe, ys, (xs - xe), (ye - ys));
                    else g.DrawRectangle(myPen, xe, ye, (xs - xe), (ys - ye));
                }
            }
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;

            action = false;
            //SetCursor(c1);
            Cursor.Current = this.c1;
            //showStatus("Mandelbrot-Set will be produced - please wait...");
            MessageBox.Show("Mandelbrot-Set will be produced - please wait...");
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                        ///djm added
                        ///HSBcol.fromHSB(h,0.8f,b); //convert hsb to rgb then make a Java Color
                        ///Color col = new Color(0,HSBcol.rChan,HSBcol.gChan,HSBcol.bChan);
                        ///g1.setColor(col);
                        //djm end
                        //djm added to convert to RGB from HSB

                     //   g1.setColor(Color.getHSBColor(h, 0.8f, b));
                        //djm test
                     //   Color col = Color.getHSBColor(h, 0.8f, b);
                    //    int red = col.getRed();
                    //    int green = col.getGreen();
                    //    int blue = col.getBlue();
                        //djm 
                        alt = h;
                    }
               //     g1.drawLine(x, y, x + 1, y);
                }
          //  showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
          //  setCursor(c2);
            action = true;
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            //e.consume();
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                //repaint();
                Refresh();
            }
            
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
                   int z, w;

           // e.consume();
                   if (action)
                   {
                       xe = e.X;
                       ye = e.Y;
                       if (xs > xe)
                       {
                           z = xs;
                           xs = xe;
                           xe = z;
                       }
                       if (ys > ye)
                       {
                           z = ys;
                           ys = ye;
                           ye = z;
                       }
                       w = (xe - xs);
                       z = (ye - ys);
                       if ((w < 2) && (z < 2)) initvalues();
                       else
                       {
                           if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                           else xe = (int)((float)xs + (float)z * xy);
                           xende = xstart + xzoom * (double)xe;
                           yende = ystart + yzoom * (double)ye;
                           xstart += xzoom * (double)xs;
                           ystart += yzoom * (double)ys;
                       }
                       xzoom = (xende - xstart) / (double)x1;
                       yzoom = (yende - ystart) / (double)y1;
                       mandelbrot();
                       rectangle = false;
                       //repaint();
                       Refresh();
                   }
        }

        private void Form1_MouseEnter(object sender, EventArgs e)
        {

        }

        private void Form1_MouseLeave(object sender, EventArgs e)
        {

        }

       private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
           // e.consume();
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                Refresh();
                //repaint();
            }
  
        }

    }
}
