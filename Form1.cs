﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fractal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            // initializes init();
            init();
            // sets width of form
            this.Width = 656;
            // sets height of form
            this.Height = 480;
            // initializes start();
            start();
            // initializes destroy();
            destroy();
        }

        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static Boolean action, rectangle, finished;
        private static float xy;
        //djm original Image picture; ---now replaced with PictureBox1 added by visual control
        private Graphics g1;
        private Cursor c1, c2;
        private Bitmap pictureBitmap;

        public void init()
        {
            // sets a width value
            this.Width = 640;
            // sets a height value
            this.Height = 400;
            finished = false;
            //djm addMouseListener(this); ---now replaced with C# mouse controls
            //djm addMouseMotionListener(this); ---now replaced with C# mouse controls
            //djm c1 = new Cursor(Cursor.WAIT_CURSOR); ---now replaced with c1 = Cursors.WaitCursor;
            c1 = Cursors.WaitCursor;
            //djm c2 = new Cursor(Cursor.CROSSHAIR_CURSOR); ---now replaced with c2 = Cursors.Cross;
            c2 = Cursors.Cross;
            //djm x1 = getSize().width; ---now replaced with x1 = this.Width
            x1 = this.Width;
            //y1 = getSize().height; ---now replaced with y1 = this.Height;
            y1 = this.Height;
            xy = (float)x1 / (float)y1;
            //djm picture = createImage(x1, y1); ---now replaced with pictureBitmap = new Bitmap(x1, y1);
            pictureBitmap = new Bitmap(x1, y1);
            //djm g1 = picture.getGraphics(); --now replaced with g1 = Graphics.FromImage(pictureBitmap);
            g1 = Graphics.FromImage(pictureBitmap);
            // displays the Bitmap on the PictureBox
            pictureBox1.Image = pictureBitmap;
            //djm finished = true; ---moved to private void exitToolStripMenuItem_Click
        }

        public void destroy() // delete all instances 
        {
            if (finished)
            {
                //djm removeMouseListener(this); ---now replaced with C# mouse controls
                //djm removeMouseMotionListener(this); ---now replaced with C# mouse controls
                //djm picture = null; ---now replaced with pictureBox1 = null;
                pictureBox1 = null;
                g1 = null;
                c1 = null;
                c2 = null;
                //djm System.gc(); ---now replaced with System.GC.Collect(); for garbage collection
                System.GC.Collect(); 
            }
        }

        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;
            action = false;
            //djm SetCursor(c1); ---now replaced with Cursor.Current = this.c2; and moved to pictureBox1_MouseUp
            //djm showStatus("Mandelbrot-Set will be produced - please wait..."); ---now replaced with toolStripStatusLabel1.Text and moved to pictureBox1_MouseDown
            // set a colour to black
            Color colour = Color.Black;
            // creates myPen and sets the colour
            Pen myPen = new Pen(colour); 
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    // get colour value for this x,y point
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y);
                    // if colour value changed then calculate new colour
                    if (h != alt)
                    {
                        // brightnes
                        b = 1.0f - h * h; 
                        /* 
                        djm added
                        HSBcol.fromHSB(h,0.8f,b); //convert hsb to rgb then make a Java Color
                        Color col = new Color(0,HSBcol.rChan,HSBcol.gChan,HSBcol.bChan);
                        g1.setColor(col);
                        djm end
                        djm added to convert to RGB from HSB
                        g1.setColor(Color.getHSBColor(h, 0.8f, b));
                        djm test
                        Color col = Color.getHSBColor(h, 0.8f, b);
                        int red = col.getRed();
                        int green = col.getGreen();
                        int blue = col.getBlue();
                        djm ended ---now replaced with colour = HSBColor.FromHSB(new HSBColor(h * 255, 0.8f * 255, b * 255));
                        */
                        // calls the HSB class and calculates colour from HSB.cs and stores it in colour
                        colour = HSBColor.FromHSB(new HSBColor(h * 255, 0.8f * 255, b * 255));
                        // uses myPen and sets the colour
                        myPen = new Pen(colour);
                        alt = h;
                    }
                    // draws the fractal
                    g1.DrawLine(myPen, x, y, x + 1, y);
                }

           //djm showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse."); ---now replaced with toolStripStatusLabel1.Text and moved to pictureBox1_MouseUp
           //djm setCursor(c2); ---now replaced with Cursor.Current = this.c2; and moved to pictureBox1_MouseMove
           action = true;
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        } 

        private void pictureBox1_Paint(object sender, PaintEventArgs e) // draws rectangle to pictureBox1
        {
            Graphics g1 = e.Graphics;

            if (rectangle)
            {
                //djm g.setColor(Color.White); ---now replaced with Pen myPen = new Pen(Color.White);
                Pen myPen = new Pen(Color.White);
                if (xs < xe)
                {
                    if (ys < ye) g1.DrawRectangle(myPen, xs, ys, (xe - xs), (ye - ys));
                    else g1.DrawRectangle(myPen, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g1.DrawRectangle(myPen, xe, ys, (xs - xe), (ye - ys));
                    else g1.DrawRectangle(myPen, xe, ye, (xs - xe), (ys - ye));
                }

            }

        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e) // occurs when mouse is clicked
        {
            //djm e.consume(); ---not needed
            toolStripStatusLabel1.Text = "Mandelbrot-Set will be produced - please wait...";
            if (action)
            {
                xs = e.X;
                ys = e.Y;
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e) // occurs when mouse is released
        {
            toolStripStatusLabel1.Text = "Mandelbrot-Set ready - please select zoom area with pressed mouse.";
            Cursor.Current = this.c1; 
            int z, w;
            //djm e.consume(); ---not needed
            if (action)
            {

                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                //djm repaint(); ---now replaced with Refresh();
                Refresh();
            }


        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e) // occurs when mouse is moved
        {

           Cursor.Current = this.c2;
           //djm e.consume(); ---not needed
           // checks if left mouse button is pressed before moving the mouse
           if (e.Button == System.Windows.Forms.MouseButtons.Left)
           {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                //djm repaint(); ---now replaced with Refresh();   
                Refresh();

            }

        }

        private void jPGToolStripMenuItem_Click(object sender, EventArgs e) // menu item jpg on click
        {
            // saves the bitmap as a JPG
            pictureBitmap.Save("H:\\Fractal.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            // displays
            toolStripStatusLabel1.Text = "Saved as JPG";
        }

        private void gIFToolStripMenuItem_Click(object sender, EventArgs e) // menu item gif on click
        {
            // saves the bitmap as a GIF
            pictureBitmap.Save("H:\\Fractal.gif", System.Drawing.Imaging.ImageFormat.Gif);
            // displays
            toolStripStatusLabel1.Text = "Saved as GIF";
        }

        private void pNGToolStripMenuItem_Click(object sender, EventArgs e) // menu item png on click
        {
            // saves the bitmap as a PNG
            pictureBitmap.Save("H:\\Fractal.png", System.Drawing.Imaging.ImageFormat.Png);
            // displays
            toolStripStatusLabel1.Text = "Saved as PNG";
        }

        private void bMPToolStripMenuItem_Click(object sender, EventArgs e) // menu item bmp on click
        {
            // saves the bitmap as a BMP
            pictureBitmap.Save("H:\\Fractal.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
            // displays
            toolStripStatusLabel1.Text = "Saved as BMP";
        }

        public String getAppletInfo() // stores a string of text in getAppletInfo
        {
            return "Original: fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001. Converted: To C# by Mark Ellis";
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e) // status strip about on click
        {
            // displays
            toolStripStatusLabel1.Text = getAppletInfo();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) // status strip exit on click
        {
            finished = true;
            // closes the program
            this.Close();
        }

    }
}
